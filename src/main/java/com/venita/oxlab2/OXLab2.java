/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.venita.oxlab2;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class OXLab2 {
  
  static char [][] board = new char[3][3]; 
  private static char currentPlayer = 'X';
  static Scanner sc = new Scanner(System.in);
  private static int row;
  private static int col; 

    public static void main(String[] args) {   
        printWelcome();
        initializeBoard();
        while (true) {
            printBorad();
            printTurnAndinputMove();
             if (isWinner()) {
                    printBorad();
                    printWinner();
                    break;
                }
             if (isDraw()) {
                    printBorad();
                    printDraw();
                    break;
                }

             switchTurn();
             
        }
        
        
    }
    
    public static void printWelcome() {
        System.out.println("Welcome to OX");  
        
    }
   public static void printBorad() {
        
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (j == 0) {
                    System.out.print(" " + board[i][j] + " ");
                } else {
                    System.out.print(" |" + board[i][j] + " ");
                }
            }
           if (i != 2) {
                System.out.println("\n-------------");
            }
           
        }
        System.out.println("\n-------------");
        
    }
private static void initializeBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = ' ';
            }
        }
        
    }
    
       static void printTurnAndinputMove() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Turn " + currentPlayer + ", enter your move (row col): ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (board[row - 1][col - 1] == ' ') {
                board[row - 1][col - 1] = currentPlayer;
                return;
            }
        }
       }
        private static void switchTurn() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }
         private static void printWinner() {
        System.out.println( " ' "+currentPlayer +" ' "+ "won!");
    }
        
 private static boolean isWinner() {
        return checkRow() || checkCol() || checkX1() || checkX2();
    }
 
 private static void printDraw() {
        System.out.println("It's a draw!");
    }

    public static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == ' ') {
                    return false;
                }
            }
        }
        return true;
    }


    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == currentPlayer && board[i][1] == currentPlayer && board[i][2] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (board[0][i] == currentPlayer && board[1][i] == currentPlayer && board[2][i] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkX1() {
        if (board[0][0] == currentPlayer && board[1][1] == currentPlayer && board[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkX2() {
        if (board[0][2] == currentPlayer && board[1][1] == currentPlayer && board[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }


}
    




   

